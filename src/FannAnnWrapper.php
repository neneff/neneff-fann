<?php


namespace Neneff\Fann;

use Neneff\Tools\Date;
use Neneff\Tools\Sql;
use Neneff\Tools\Variable;

class FannAnnWrapper
{

    /**
     * @param \PDO $pdo
     * @param integer $trainingSessionId
     * @param array   $values
     * @return array
     * @throws
     */
    static public function testValues(\PDO $pdo, $trainingSessionId, array $values)
    {
        $stmt = $pdo->query("
          SELECT trained_binary
          FROM   ann_training_session
          WHERE  id = {$trainingSessionId}
        ");
        $blob = $stmt->fetchColumn();

        // --
        $stmt = $pdo->query("
            SELECT ann_session_field.*
            FROM   ann_training_session
            LEFT JOIN ann_session_field 
                   ON ann_session_field.session_id = ann_training_session.session_id 
            WHERE  ann_training_session.id     = {$trainingSessionId}
            ORDER BY ann_session_field.id
        ");
        $fields  = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $inputs  = [];
        $outputsFields = [];

        // -- (Input Value – minimal_input_value) / (maximal_input_value - minimal_input_value)
        // -- -- *2.5 to have a little more scale available
        $normalize = function($value, $min, $max) {
            return (($value - $min) / ($max - $min)) *2.5;
        };

        foreach($fields as $field)
        {
            if($field['is_result'] == 0)
            {
                $input = $values[$field['id']];
                if(isset($field['normal_min']) && isset($field['normal_max']))
                {
                    $input = $normalize($input, $field['normal_min'], $field['normal_max']);
                }
                else
                    {
                    $input = (float)$input;
                }
                $inputs[] = $input;
            }
            else
            {
                $outputsFields[] = $field;
            }
        }

        $tmpName = sys_get_temp_dir().DIRECTORY_SEPARATOR.uniqid(date("Ymd_His_").$trainingSessionId);
        file_put_contents($tmpName, $blob);

        // -- run ANN
        $ann  = fann_create_from_file($tmpName);
        unlink($tmpName);

        if($ann)
        {
            $preciates = fann_run($ann, $inputs);
            fann_destroy($ann);
        }
        else
        {
            throw new \Exception(fann_get_errstr($ann), fann_get_errno($ann));
        }
        $results = [];

        foreach($outputsFields as $index => $outputsField)
        {
            $results[$outputsField['id']] = $preciates[$index];
        }
        return $results;
    }


    /**
     * @param  \PDO     $pdo
     * @param  integer  $trainingSessionId
     * @return double
     * @throws
     */
    static public function testTrainingSession(\PDO $pdo, $trainingSessionId, $settingFilePath)
    {
        $stmt = $pdo->query("
          SELECT session_id, trained_binary
          FROM   ann_training_session
          WHERE  id = {$trainingSessionId}
        ");
        $sessionId = $stmt->fetch(\PDO::FETCH_COLUMN);

        // -- Get Training data
        $stmt = $pdo->query("
          SELECT is_result
          FROM   ann_session_field
          WHERE  session_id = {$sessionId}
          ORDER BY id
        ");
        $fields = $stmt->fetchAll(\PDO::FETCH_COLUMN);

        $stmt = $pdo->query("
          SELECT v.row_id, normalized_value
          FROM   ann_session_value v
            JOIN ann_session_row   r ON r.id = v.row_id
          WHERE  v.session_id    = {$sessionId}
            AND  r.is_train_data = 1
          ORDER BY v.row_id, v.field_id
        ");
        $rows = $stmt->fetchAll(\PDO::FETCH_GROUP | \PDO::FETCH_COLUMN);

        if($info = self::_createFileFromDataSet($fields, $rows, sys_get_temp_dir().DIRECTORY_SEPARATOR.uniqid(date("Ymd_His").'_test_data_set_'.$trainingSessionId)))
        {
            $ann = fann_create_from_file($settingFilePath);
            if($ann)
            {
                $data = fann_read_train_from_file($info['file-path']);
                $test = fann_test_data($ann, $data);

                fann_destroy($ann);

                @unlink($settingFilePath);
                @unlink($info['file-path']);

                return $test;
            }
            else {
                throw new \Exception(fann_get_errstr($ann), fann_get_errno($ann));
            }
        }
        else {
            throw new \Exception('Unable to create training session file');
        }
    }


    /**
     * @param \PDO    $pdo
     * @param integer $trainSessionId
     * @param array   $hiddenLayers
     * @param integer $hiddenLayersOutputFunction
     * @param integer $outputFunction
     * @param integer $maxEpoch
     * @param integer $epochBetweenReport
     * @param float   $desiredError
     *
     * @throws
     */
    static public function performTrainingSession(\PDO $pdo, $trainSessionId, $hiddenLayers, $hiddenLayersOutputFunction=FANN_SIGMOID_SYMMETRIC, $outputFunction=FANN_SIGMOID_SYMMETRIC, $maxEpoch=100, $epochBetweenReport=5, $desiredError=0.001)
    {
        // -- Get Training data
        $stmt = $pdo->query("
          SELECT neurons_input, neurons_output, training_dataset
          FROM   ann_training_session
          WHERE  id = {$trainSessionId}
        ");
        $trainingSession = $stmt->fetch(\PDO::FETCH_ASSOC);

        if(isset($trainingSession['training_dataset']))
        {
            $tmpName = sys_get_temp_dir().DIRECTORY_SEPARATOR.uniqid(date("Ymd_His_").$trainSessionId);
            file_put_contents($tmpName, $trainingSession['training_dataset']);

            $layers = array_merge([$trainingSession['neurons_input']], $hiddenLayers, [$trainingSession['neurons_output']]);
            $ann    = fann_create_standard_array(count($layers), $layers);

            if($ann)
            {
                fann_set_activation_function_hidden($ann, $hiddenLayersOutputFunction);
                fann_set_activation_function_output($ann, $outputFunction);

                if (fann_train_on_file($ann, $tmpName, $maxEpoch, $epochBetweenReport, $desiredError))
                {
                    $resultFile = sys_get_temp_dir().DIRECTORY_SEPARATOR.date('Ymd_His_').$trainSessionId.'_result';
                    fann_save($ann, $resultFile);
                    fann_destroy($ann);

                    if(file_exists($resultFile))
                    {
                        $blob     = fopen($resultFile, 'rb');
                        $row      = Date::getCurrentDateTimeUTC();
                        $accuracy = self::testTrainingSession($pdo, $trainSessionId, $resultFile);

                        $stmt = $pdo->prepare("
                            UPDATE ann_training_session
                              SET trained_binary = :blob,
                                  trained_at     = '{$row}',
                                  accuracy       =  {$accuracy}
                            WHERE id = {$trainSessionId}
                        ");
                        $stmt->bindParam(':blob', $blob, \PDO::PARAM_LOB);
                        $stmt->execute();
                    }
                    else {
                        throw new \Exception("The file {$resultFile} does not exist, the training probably failed : ".fann_get_errstr($ann), fann_get_errno($ann));
                    }
                }
                else {
                    throw new \Exception(fann_get_errstr($ann), fann_get_errno($ann));
                }
            }
            else {
                throw new \Exception(fann_get_errstr($ann), fann_get_errno($ann));
            }
        }
        else {
            throw new \Exception("The date set for trainig does not exist, training session cannot be performed");
        }
    }


    /**
     * @param \PDO $pdo
     * @param integer $sessionId
     * @param string  $pathToSaveFiles
     *
     * @return integer
     * @throws
     */
    static public function prepareTrainingSession(\PDO $pdo, $sessionId, $pathToSaveFiles)
    {
        // -- Get Training data
        $stmt = $pdo->query("
          SELECT is_result
          FROM   ann_session_field
          WHERE  session_id = {$sessionId}
          ORDER BY id
        ");
        $fields = $stmt->fetchAll(\PDO::FETCH_COLUMN);

        $stmt = $pdo->query("
          SELECT v.row_id, normalized_value
          FROM   ann_session_value v
            JOIN ann_session_row   r ON r.id = v.row_id
          WHERE  v.session_id    = {$sessionId}
            AND  r.is_train_data = 0
          ORDER BY v.row_id, v.field_id
        ");
        $rows = $stmt->fetchAll(\PDO::FETCH_GROUP | \PDO::FETCH_COLUMN);

        if($info = self::_createFileFromDataSet($fields, $rows, $pathToSaveFiles.DIRECTORY_SEPARATOR.date('Ymd_His').'_training_'.$sessionId))
        {
            // -- save training initial status
            $blob = fopen($info['file-path'], 'rb');
            $now  = Date::getCurrentDateTimeUTC();
            $stmt = $pdo->prepare("
                INSERT INTO ann_training_session(session_id, training_dataset, neurons_input, neurons_output, created_at)
                VALUES ({$sessionId}, :blob, {$info['nb-inputs']}, {$info['nb-outputs']}, '{$now}')
            ");
            $stmt->bindParam(':blob', $blob, \PDO::PARAM_LOB);
            $stmt->execute();

            // -- delete temp file
            unlink($info['file-path']);

            return (int)$pdo->lastInsertId();
        }
        else
        {
            throw new \Exception('Unable to create training session file');
        }
    }


    /**
     * @param array $fields
     * @param array $dataSet
     * @param string $path
     * @return array
     * @throws
     */
    static protected function _createFileFromDataSet($fields, $dataSet, $path)
    {
        if(count($dataSet) > 0)
        {
            // -- Prepare date to be written in file
            $tmpRows = [];
            foreach($dataSet as $row)
            {
                $tmpRow = [];
                foreach($row as $index => $datum)
                {
                    if($fields[$index] === 0)
                    {
                        $tmpRow['inputs'][] = $datum;
                    }
                    else {
                        $tmpRow['outputs'][] = $datum;
                    }
                }
                $tmpRows[] = $tmpRow;
            }

            $rows      = $tmpRows;
            $nbRows    = count($rows);
            $nbInputs  = count($rows[0]['inputs']);
            $nbOutputs = count($rows[0]['outputs']);
            $file      = new \SplFileObject($path, 'w');

            // -- Write Header
            $file->fwrite("{$nbRows} {$nbInputs} {$nbOutputs}".PHP_EOL);

            // -- Write Data
            foreach($rows as $row)
            {
                $file->fwrite(join(' ', $row['inputs']).PHP_EOL);
                $file->fwrite(join(' ', $row['outputs']).PHP_EOL);
            }

            $path = $file->getRealPath();
            $file = null;

            return [
                'nb-inputs'  => $nbInputs,
                'nb-outputs' => $nbOutputs,
                'file-path'  => $path,
            ];
        }
        else
        {
            throw new \Exception('There is no data to train');
        }
    }


    /**
     * Will create 2D table with scaled data when needed
     * @param \PDO   $pdo
     * @param string $tableName
     * @param string $fieldNameResult
     * @param array  $fieldsNamesToIgnore
     * @param array  $fieldsNamesToScale
     * @param array  $fieldsNamesToEnum
     * @param float  $trainToTestPercentage <p>percentage of rows marked as train or test</p>
     * @param string $enumQualifier [OPTINAL] default=':ENUM:' <p>string used to mark enum values, will be used to refer the original column in the 2D DB values</p>
     * @return integer <p>The new session Id generated</p>
     * @throws
     */
    static public function prepareDataFromTable(\PDO $pdo, $tableName, $fieldNameResult, array $fieldsNamesToIgnore = [], array $fieldsNamesToScale = [], array $fieldsNamesToEnum = [], $trainDataPercentage=80.0, $enumQualifier='enum:')
    {
        // --
        $stmt = $pdo->query("SELECT * FROM {$tableName}");
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        // TODO -- remove $fieldsNamesToIgnore from  $fieldsNamesToScale && $fieldsNamesToEnum to avoid conflict (or throw it ?)
        // TODO -- remove $fieldsNamesToEnum from $fieldsNamesToScale to avoid conflict (or throw it ?)

        // -- Scale required fields
        // -- -- it will normalize the value to optimize the ANN training afterward
        // -- -- the ann_session_field will have the normal_modifier
        // -- -- Both original value and normalized value will be set at ann_session_value level
        $normalizeRelations = [];
        if(count($fieldsNamesToScale) > 0)
        {
            $sql = [];
            foreach($fieldsNamesToScale as $fieldNameToScale)
            {
                $sql[] = "SELECT '{$fieldNameToScale}' as `field`, MAX($fieldNameToScale) as `max`, MIN($fieldNameToScale) as `min`  FROM {$tableName} ";
            }
            $normalizeRelations = $pdo->query(join(' UNION ', $sql))->fetchAll( \PDO::FETCH_GROUP | \PDO::FETCH_UNIQUE | \PDO::FETCH_ASSOC);
        }

        // -- Automatically Split these fields as enum,
        // -- -- it will virtually create fields ([original_name]_[distinct_value]) with the distinct values for each fields
        // -- -- and populate them with Boolean value to be readable by the ANN (0/1)
        // -- -- The original field will not be use in the final structure, and will only be ref un the ann_session_field.original_name
        $enumRelations = [];
        if(count($fieldsNamesToEnum) > 0)
        {
            $sql = [];
            foreach($fieldsNamesToEnum as $fieldNameToEnum)
            {
                $sql[] = "SELECT DISTINCT '{$fieldNameToEnum}' as `field`, LOWER({$fieldNameToEnum}) as `value` FROM {$tableName} ";
            }

            $results = $pdo->query(join(' UNION ', $sql))->fetchAll( \PDO::FETCH_GROUP |\PDO::FETCH_NUM | \PDO::FETCH_COLUMN);
            foreach($results as $column => $enums)
            {
                foreach($enums as $enum)
                {
                    $enumRelations[$column][$enum] = $enumQualifier.$column.':'.$enum;
                }
            }
        }

        // -- (Input Value – minimal_input_value) / (maximal_input_value - minimal_input_value)
        // -- -- *2.5 to have a little more scale available
        $normalize = function($value, $min, $max) {
            return (($value - $min) / ($max - $min)) *2.5;
        };

        // -- Refactor data set with the definition (enum, scale, ignore)
        $refactoredData = [];
        foreach($data as $index => $row)
        {
            $refactoredRow = [];
            foreach($row as $column => $datum)
            {
                if(in_array($column, $fieldsNamesToIgnore))
                {
                    // -- swallow
                }
                else if(isset($enumRelations[$column]))
                {
                    foreach($enumRelations[$column] as $enum => $newColumn)
                    {
                        $refactoredRow[$newColumn] = [
                            'normalized' => ($enum == strtolower($datum)) ? 1 : 0,
                            'original'   => $datum,
                            'type'       => $enumQualifier.$column,
                        ];
                    }
                }
                else
                {
                    $refactoredRow[$column] = [
                        'normalized' => isset($normalizeRelations[$column]) ? $normalize($datum, $normalizeRelations[$column]['min'], $normalizeRelations[$column]['max']) : $datum,
                        'original'   => $datum,
                        'type'       => gettype($datum),
                    ];
                }
            }
            $refactoredData[] = $refactoredRow;
        }

        $data = $refactoredData;
        if(count($data) > 0)
        {
            foreach($data as $index => $row)
            {
                foreach($row as $fieldName => $values)
                {
                    if(!in_array(gettype($values['normalized']), ['boolean', 'integer', 'double']))
                    {
                        throw new \Exception("Wrong type detected on row {$index} for field {$fieldName} with value ".$values['normalized']);
                    }
                }
            }

            try {
                // -- Prepare new session
                $pdo->beginTransaction();
                $pdo->exec("
                  INSERT INTO ann_session (original_table)
                  VALUES('{$tableName}')
                ");
                $sessionId = (int)$pdo->lastInsertId();

                // -- Prepare Session fields references
                $fieldsData = [];
                foreach (array_keys($data[0]) as $fieldName)
                {
                    $fieldsData[] = $sessionId;   // session_id
                    $fieldsData[] = $fieldName;   // field_name
                    $fieldsData[] = Variable::issetOrNull($normalizeRelations[$fieldName]['min']);  // normal_min
                    $fieldsData[] = Variable::issetOrNull($normalizeRelations[$fieldName]['max']);  // normal_max
                    $fieldsData[] = $fieldNameResult == $fieldName ? 1 : 0;  // is_result
                }

                // -- Insert Fields
                $fieldLists   = "session_id, field_name, normal_min, normal_max, is_result";
                $placeholders = Sql::preparePlaceholdersList(count($fieldsData), count(explode(',', $fieldLists)));
                $stmt         = $pdo->prepare("
                  INSERT INTO ann_session_field ({$fieldLists})
                  VALUES {$placeholders}
                ");
                $stmt->execute($fieldsData);
                $fields = $pdo->query("
                  SELECT field_name, ann_session_field.* 
                  FROM   ann_session_field 
                  WHERE  session_id = {$sessionId}"
                )->fetchAll(\PDO::FETCH_ASSOC | \PDO::FETCH_GROUP | \PDO::FETCH_UNIQUE);

                $rowData   = [];
                $valueData = [];
                foreach($data as $rowIndex => $rowOriginalData)
                {
                    $rowData[] = $sessionId; // session_id
                    $rowData[] = $rowIndex;  // row_position
                    $rowData[] = rand(0, 100) < $trainDataPercentage ? 1 : 0; // is_train_data

                    foreach($rowOriginalData as $fieldName => $values)
                    {
                        $fieldId = $fields[$fieldName]['id'];

                        $valueData[] = $sessionId;             // session_id
                        $valueData[] = $fieldId;               // field_id
                        $valueData[] = $rowIndex;              // row_position
                        $valueData[] = $values['type'];        // type
                        $valueData[] = $values['normalized'];  // normalized_value
                        $valueData[] = $values['original'];    // original_value
                    }
                }

                // -- Insert Rows
                $fieldLists = "session_id, row_position, is_train_data";
                SQL::simpleChunkedQuery($pdo, $rowData, count(explode(',', $fieldLists)), function($pdo, $placeholders) use ($fieldLists)
                {
                    return $pdo->prepare("
                      INSERT INTO ann_session_row ({$fieldLists})
                      VALUES {$placeholders}
                    ");
                });

                // -- Insert Values
                $fieldLists = "session_id, field_id, row_position, type, normalized_value, original_value";
                SQL::simpleChunkedQuery($pdo, $valueData, count(explode(',', $fieldLists)), function($pdo, $placeholders) use ($fieldLists)
                {
                    return $pdo->prepare("
                      INSERT INTO ann_session_value ({$fieldLists})
                      VALUES {$placeholders}
                    ");
                });

                // -- Link values with its rows
                $pdo->exec("
                  UPDATE ann_session_value 
                   JOIN  ann_session_row ON ann_session_value.session_id   = ann_session_row.session_id
                                        AND ann_session_value.row_position = ann_session_row.row_position
                  SET  ann_session_value.row_id = ann_session_row.id
                  WHERE ann_session_value.session_id = {$sessionId}
                ");

                // -- Link fields with their types
                $pdo->exec("
                  UPDATE ann_session_field
                   JOIN  (
                    SELECT field_id, GROUP_CONCAT(DISTINCT type) as types
                    FROM   ann_session_value
                    WHERE  session_id = {$sessionId}
                    GROUP BY field_id
                  ) values_types
                    ON values_types.field_id = ann_session_field.id
                  SET  ann_session_field.value_types = values_types.types
                  WHERE ann_session_field.session_id = {$sessionId}
                ");
                $pdo->commit();

                return $sessionId;
            }
            catch(\Exception $e)
            {
                $pdo->rollBack();
                throw $e;
            }
        }
        else
        {
            throw new \Exception("There is no data to be extracted in {$tableName}");
        }
    }



}